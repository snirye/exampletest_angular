import { Test1Service } from '../../test1.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {


  @Output() addUser:EventEmitter<any> = new EventEmitter<any>();
  @Output() addUserPs:EventEmitter<any> = new EventEmitter<any>();
  service:Test1Service;
  usrform = new FormGroup({
      name:new FormControl(),
      phonenumber:new FormControl()


/*

  @Output()
  addUser: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  addUserPs: EventEmitter<any> = new EventEmitter<any>();

  service:Test1Service;
  
  usrform = new FormGroup({
    name: new FormControl('', Validators.required),
    phonenumber: new FormControl('', Validators.required)

*/

  });

/*


  sendData(){
    this.addUser.emit(this.usrform.value.name);
    this.service.postUser(this.usrform.value).subscribe(response =>{
    this.addUserPs.emit();
    console.log(response);
    });
  }

  */

sendData() {
    //השורה שולחת את התוכן לאופטימיסטיק שנמצא באב
    //this.username = this.addform.value.username;
    //this.email = this.addform.value.email;
    this.addUser.emit(this.usrform.value.name); //עדכון לאב בהתאם לשדות שהגדרנו למעלה
    //this.addUser.emit(this.addform.value.email); //עדכון לאב בהתאם לשדות שהגדרנו למעלה 
    
    console.log(this.usrform.value);
    this.service.postUser(this.usrform.value).subscribe(
      response => {
        console.log(response.json());
        this.addUserPs.emit();
      }
    );
  }

/*
  constructor(service:Test1Service) { 
    this.service = service;
  }


*/

  constructor(service: Test1Service, private formBuilder:FormBuilder) { 
    this.service = service;
  }


  ngOnInit() {
  }

  

}


  
