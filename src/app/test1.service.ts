import { environment } from './../environments/environment';                    //*** build ****/
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { HttpParams, HttpHeaders } from '@angular/common/http';
import { AngularFireDatabase } from 'angularfire2/database';


@Injectable()
export class Test1Service {

  getUsers(){
    //get users from the SLIM rest API (Don't say DB)
    //return  this.http.get('http://localhost/angular/Test/Slim/users');
    return  this.http.get(environment.url + 'users');                                   //*** build ****/
  }

  getUsersFire(){
    return this.db.list('/users').valueChanges();    
  }


/*

  postUser(data){
    let options = {
      headers: new Headers({'content-type': 'application/x-www-form-urlencoded'}
    )};
    var params = new HttpParams().append('name',data.name).append('phonenumber',data.phonenumber);
    return this.http.post('http://localhost/angular/Test/Slim/users',params.toString(),options); 
  }

  */

 postUser(data){
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    //בגרש נגדיר את השדות בהתאם לערכים שהגדרנו בשרת
    let params = new HttpParams().append('name',data.name).append('phonenumber',data.phonenumber);
    //return this.http.post('http://localhost/angular/Test/Slim/users', params.toString(), options);
    return this.http.post(environment.url + 'users',params.toString(),options);                       //*** build ****/
  }





    deleteUser(id){
    //let options = {
    //  headers: new Headers({'content-type': 'application/x-www-form-urlencoded'}
    //)}; 
    console.log('http://localhost/angular/Test/Slim/users/'+id);   
    //return this.http.delete('http://localhost/angular/Test/Slim/users/'+id); 
     return this.http.delete(environment.url + 'users/'+id);                                       //*** build ****/
  }

//Get one user
 getUser(id){
  //return this.http.get('http://localhost/angular/Test/Slim/users/'+ id);
   return this.http.get(environment.url + 'users/'+id);                                       //*** build ****/
}

//Put
putUser(data,key){
 let options = {
   headers: new Headers({
     'content-type':'application/x-www-form-urlencoded'
   })
 }
 //בגרש נגדיר את השדות בהתאם לערכים שהגדרנו בשרת
 let params = new HttpParams().append('name',data.name).append('phonenumber',data.phonenumber);
 //return this.http.put('http://localhost/angular/Test/Slim/users/'+ key,params.toString(), options);
 return this.http.put(environment.url + 'users/'+ key,params.toString(), options);                  //*** build ****/
}


  constructor(private http:Http, private db:AngularFireDatabase) { 
    
  }
  
}

