import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FireUsersComponent } from './fire-users.component';

describe('FireUsersComponent', () => {
  let component: FireUsersComponent;
  let fixture: ComponentFixture<FireUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FireUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FireUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
