import { Component, OnInit } from '@angular/core';
import { Test1Service } from "../test1.service";

@Component({
  selector: 'fire-users',
  templateUrl: './fire-users.component.html',
  styleUrls: ['./fire-users.component.css']
})
export class FireUsersComponent implements OnInit {

  users;

  constructor(private service:Test1Service) { }

  ngOnInit() {
    this.service.getUsersFire().subscribe(users =>{
      this.users = users;
      console.log(this.users);
    });
  }

}