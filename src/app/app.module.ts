import { UpdateFormComponent } from './users/update-form/update-form.component';
import { UserComponent } from './users/user/user.component';
import { UserFormComponent } from './users/user-form/user-form.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { ProductsComponent } from './products/products.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { NavigationComponent } from './navigation/navigation.component';
import {RouterModule} from '@angular/router';
import { Test1Service } from './test1.service';
import { HttpModule } from '@angular/http';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { FireUsersComponent } from './fire-users/fire-users.component';
import {AngularFireModule} from 'angularfire2';
import {AngularFireDatabaseModule} from 'angularfire2/database';
import { environment } from './../environments/environment';





@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    ProductsComponent,
    NotFoundComponent,
    NavigationComponent,
    UserFormComponent,
    FireUsersComponent,
    UpdateFormComponent,
    UserComponent

  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    RouterModule.forRoot([
       {path:'', component:ProductsComponent},
       {path:'users', component:UsersComponent},
       {path:'fireusers', component:FireUsersComponent},
       {pathMatch:'full', path:'users-firebase', component:FireUsersComponent},
       {path: 'user/:id', component: UserComponent},
       {path: 'update-form/:id', component: UpdateFormComponent},
       {path:'**',component:NotFoundComponent}
     ])
  ],
  providers: [
    Test1Service
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
