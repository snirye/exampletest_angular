// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  url:'http://localhost/angular/Test/Slim/',
  firebase:{  
  apiKey: "AIzaSyD5oHc5TUQ8SZzs0jBwNb7Nok-a_iPXoaQ",
    authDomain: "users-1af4d.firebaseapp.com",
    databaseURL: "https://users-1af4d.firebaseio.com",
    projectId: "users-1af4d",
    storageBucket: "users-1af4d.appspot.com",
    messagingSenderId: "51636994235"
  }
};
